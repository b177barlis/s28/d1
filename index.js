// JavaScript Synchronous vs Asynchronous
// Synchronous - statements are executed one at a time

console.log("Hello World");
// conosle.log("Hello Again");
console.log("Goodbye");


// Asynchronous - statements can proceed to execute while other codes are running in the background


// Fetch API - allows to asynchronously request for a resoucrce (data)
// Get all post

console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

// Check the status of the request
/*
	Syntax:
		fetch('URL')
		.then((response))=>{})
*/

fetch('https://jsonplaceholder.typicode.com/posts')
.then(response => console.log(response.status));

// Retrieve the contents/data from the "Response" object

fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json())
.then((json) => console.log(json));

// Create a function using "async" and "await" keywords.
// "async" and "await" can be used to achieve asynchronous code

async function fetchData(){
	let result = await fetch('https://jsonplaceholder.typicode.com/posts');
	console.log(result)

	console.log(typeof result);

	let json = await result.json();

	console.log(json);
}

fetchData();